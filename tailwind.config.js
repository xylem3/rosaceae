/* eslint-env node */
/** @type {import("tailwindcss").Config} */
module.exports = {
    theme: {
        fontFamily: {
            sans: ["Source Sans Pro", "Roboto", "Arial", "sans-serif"],
        },
    },
    plugins: [],
};
