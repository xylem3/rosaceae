// https://nuxt.com/docs/api/configuration/nuxt-config
// noinspection JSUnusedGlobalSymbols
export default defineNuxtConfig({
    postcss: {
        plugins: {
            tailwindcss: {},
            autoprefixer: {},
        },
    },
    typescript: {
        strict: true,
        typeCheck: true,
    },
    modules: [
        [
            "@nuxtjs/google-fonts",
            {
                download: true,
                families: {
                    "Source+Sans+Pro": true,
                    Roboto: true,
                },
            },
        ],
        "@nuxtjs/tailwindcss",
    ],
    app: {
        baseURL: process.env.ROSACEAE_ENV === "DEBUG" ? undefined : "/rosaceae",
    },
    srcDir: "src/",
    ssr: true,
});
