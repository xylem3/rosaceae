/* eslint-env node */
require("@rushstack/eslint-patch/modern-module-resolution");

module.exports = {
    root: true,
    parser: "vue-eslint-parser",
    parserOptions: {
        ecmaVersion: "latest",
        parser: "@typescript-eslint/parser",
    },
    extends: [
        "plugin:vue/vue3-recommended",
        "eslint:recommended",
        "@nuxtjs/eslint-config-typescript",
        "plugin:prettier/recommended",
    ],
};
